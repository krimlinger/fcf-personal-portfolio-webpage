const path = require('path');
const glob = require('glob');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin')

const templateDir = path.resolve(__dirname, 'src/templates');
const htmlPlugins = [];
glob.sync(path.join(templateDir, '**/index.hbs'),
          { absolute: true }).forEach(f => {
      const subDir = path.basename(path.dirname(f));
      htmlPlugins.push(new HtmlWebpackPlugin({
        template: f,
        filename: path.join(subDir == 'templates' ? '.': subDir,
                            path.basename(f, '.hbs') + '.html')}));
      });

module.exports = {
  entry: './src/index.js',
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'dist'),
  },
  devServer: {
    contentBase: './dist',
  },
  module: {
    rules: [
      {
        test: /\.s?css$/,
        use: [
          MiniCssExtractPlugin.loader,
          'css-loader',
          'sass-loader',
        ]
      },
      {
        test: /\.(png|jpg|jpeg|gif)$/,
        loader: 'file-loader',
        options: {
          name: '[name].[ext]',
          publicPath: '../img/',
          outputPath: 'img',
        },
      },
      {
        test: /keybase\.txt$/,
        loader: 'file-loader',
        options: {
          name: '[name].[ext]',
        },
      },
      {
        test: /\.hbs$/,
        use: ['handlebars-loader'],
      },
      {
        test: /\.(js)$/,
        exclude: /node_modules/,
        use: ['babel-loader'],
      }
    ],
  },
  resolve: {
    extensions: ['*', '.js'],
    alias: {
      'handlebars': 'handlebars/runtime.js',
    },
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: 'css/style.css',
      chunkFilename: 'css/[id].css',
    }),
    ...htmlPlugins,],
}
