import './keybase.txt'
import './img/arcdps_screenshot.png'
import './img/gasnu_screenshot.png'
import './img/product_landing_screenshot.png'
import './img/tribute_screenshot.png'
import './img/technical_documentation_screenshot.png'
import './img/survey_screenshot.png'
import 'normalize.css'
import './css/style.scss'
import { library, dom } from '@fortawesome/fontawesome-svg-core'
import { faGlobe, faEnvelope, faTag } from '@fortawesome/free-solid-svg-icons'
import { faGitlab, faKeybase } from '@fortawesome/free-brands-svg-icons'

library.add(faGitlab, faGlobe, faEnvelope, faKeybase, faTag);
dom.watch();

if ((navigator.language || navigator.userLanguage) == 'fr' &&
  window.location.pathname != "/fr_fr/") {
  window.location.replace('fr_fr/');
}

window.addEventListener('scroll', (function() {
  const navBar = document.getElementById('navbar');
  const navThreshold = 50;

  if (window.pageYOffset > navThreshold) {
    navBar.dataset.navbarScrolled = 'true';
  } else {
    navBar.dataset.navbarScrolled = 'false';
  }
  let navbarIsScrolled = window.pageYOffset >= navThreshold;
  return (function () {
    if (window.pageYOffset > navThreshold && !navbarIsScrolled) {
      navBar.dataset.navbarScrolled = navbarIsScrolled = true;
    } else if (window.pageYOffset <= navThreshold && navbarIsScrolled) {
      navBar.dataset.navbarScrolled = navbarIsScrolled = false;
    }
  });
})());

document.getElementById('contact-email').onclick = function() {
  if (this.className != 'contact-email-clicked') {
    const addr = '[firstname].[lastname]@protonmail.com';
    this.className = 'contact-email-clicked';
    this.innerHTML = `<span class="email">${addr}<span>`;
    this.firstElementChild.style.opacity = 0;
    setTimeout(x => this.firstElementChild.style.opacity = 1, 200);
  }
}

{
  let selectedProject = null;
  for (let item of document.querySelectorAll('.project-tile')) {
    item.onclick = function() {
      if (selectedProject !== null && selectedProject !== this) {
        selectedProject.classList.remove('selected-project');
      }
      if (this.classList.toggle('selected-project')) {
        selectedProject = this;
      }
    }
  }
}
