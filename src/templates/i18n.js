import I18N_CONTENT from '../i18n.json'

function getValueOrDefault(obj, key, def=undefined) {
  if (obj.hasOwnProperty(key)) {
    return obj[key];
  }
  return def;
}

function getTextOrNotify(obj, key) {
  return getValueOrDefault(obj, key, '!No text translation found!');
}

export default function(text) {
  if (this.hasOwnProperty('lang') && I18N_CONTENT.hasOwnProperty(this.lang)) {
      return getValueOrDefault(I18N_CONTENT[this.lang],
                               text,
                               getTextOrNotify(I18N_CONTENT.default, text));
  }
  return getTextOrNotify(I18N_CONTENT.default, text);
}
